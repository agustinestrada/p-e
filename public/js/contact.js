const contactForm = document.querySelector('.contact-form')

let nombre = document.getElementById('nombre')
let apellido = document.getElementById('apellido')
let email = document.getElementById('email')
let subject = document.getElementById('subject')
let comment = document.getElementById('comment')


contactForm.addEventListener('submit', (e)=>{
    e.preventDefault()
    
    let data = {
        nombre: nombre.value,
        apellido: apellido.value,
        email: email.value,
        subject: subject.value,
        comment: comment.value
    }
    
    let xhr = new XMLHttpRequest()
    
    xhr.open('POST','/contactForm')
    xhr.setRequestHeader('content-type','application/json')
    xhr.onload = function(){
        console.log(xhr.responseText)
        if (xhr.responseText == 'success') {
            alert('Email sent')

            nombre.value = ''
            apellido.value = ''
            email.value = ''
            subject.value = ''
            comment.value = ''
                    
        }else{
            alert('Something went wrong!')
        }
    }

    xhr.send(JSON.stringify(data))


   // console.log(data);
})