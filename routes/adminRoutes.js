const express = require('express')
const adminRouter = express.Router()
const adminController = require('../controllers/adminController')
const adminMiddleware = require('../middlewares/adminMiddleware')
const uploadPDF = require('../middlewares/multerPDFMiddleware')
const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
    destination: (req, file, cb) =>{
        let folder = path.join(__dirname, '../public/img/my_work')

        cb(null,folder)
    },
    filename: (req, file, cb) =>{
        const newFileName = Date.now() + path.extname(file.originalname)

        cb(null, newFileName)
    }
})

const upload = multer({storage})


adminRouter.get('/', adminController.login)
adminRouter.post('/', adminController.logeo)

adminRouter.get('/panel', adminMiddleware, adminController.panel)
adminRouter.get('/logout', adminController.logout)
adminRouter.get('/mywork', adminMiddleware, adminController.upload)
adminRouter.post('/mywork', upload.single('img'), adminController.uploadWork)
adminRouter.post('/resume', uploadPDF.single('resume'), adminController.resume)






module.exports = adminRouter

