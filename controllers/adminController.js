const path = require('path')
const fs = require('fs')
const adminModel = require('../models/admin')

const adminController = {
    login: (req,res)=>{
        res.render('admin/logeo')
    },
    logeo: (req, res)=>{
        const {admin, password} = req.body
        
        if(admin == 'estradaanimation@gmail.com' && password == '1234'){
            
            req.session.adminLogeado = {
                estaLogeado: true,
                email: 'estradaanimation@gmail.com',
                persona: 'Pablo de Estrada'
            }

            const works = adminModel.findAll()
        
            res.render('admin/panel', { works })

        }
    },
    panel: (req, res)=>{

        const works = adminModel.findAll()
        
        res.render('admin/panel', { works })
    },
    logout: (req, res)=>{
        req.session.destroy()

        res.redirect('/')
    },
    upload: (req, res) =>{
        res.render('admin/adminWorks.ejs')
    },
    uploadWork: (req, res) => {
        const { title, category,description } = req.body
        const { filename } = req.file

        const newWork = {
            id: adminModel.generateId(),
            title,
            category,
            description,
            img:filename
        }

        adminModel.create(newWork);

        res.redirect('/admin/panel')
    },
    resume: (req, res) => {
        res.redirect('/admin/panel')
    }
}

//console.log(adminController.panel());

module.exports = adminController